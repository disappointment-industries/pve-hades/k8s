#!/usr/bin/env bash

########################### Virtualenv setup ####################################

# create virtualenv if not present
[[ ! -d .venv ]] && python3 -m venv .venv

source .venv/bin/activate

pip3 install ansible ansible-lint

########################### Ansible setup ####################################

ansible-galaxy install --force -r requirements.galaxy.yml

########################### Help ####################################

echo
echo "########################################"
echo
echo "Your playbooks:"

find . -maxdepth 1 -type f -name "*.yaml" | grep -v inventory

echo
echo "Recommendation: Set up your OpenSSH config based on inventory.yml"

echo
echo "You can run playbook with:"
printf "\tansible-playbook your-playbook.yaml"
echo

# to stay in our comfy virtualenv
exec "${SHELL:-bash}" "$@"
