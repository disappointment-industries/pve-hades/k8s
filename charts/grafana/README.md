#!/bin/bash

# Add repo
    helm repo add grafana https://grafana.github.io/helm-charts
    helm repo update

# Install grafana
    helm install grafana grafana/grafana -f values.yaml