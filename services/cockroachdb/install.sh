#!/bin/sh

#CRD_MANIFEST="https://raw.githubusercontent.com/cockroachdb/cockroach-operator/master/install/crds.yaml"
CRD_MANIFEST="https://raw.githubusercontent.com/cockroachdb/cockroach-operator/v2.1.0/config/crd/bases/crdb.cockroachlabs.com_crdbclusters.yaml"
#OPERATOR_MANIFEST="https://raw.githubusercontent.com/cockroachdb/cockroach-operator/master/install/operator.yaml"
OPERATOR_MANIFEST="https://raw.githubusercontent.com/cockroachdb/cockroach-operator/v2.1.0/manifests/operator.yaml"
kubectl create ns cockroach
kubectl apply -f "$CRD_MANIFEST"
curl "$OPERATOR_MANIFEST" | sed 's/namespace: default/namespace: cockroach/g' | kubectl apply -f -
sleep 10
echo "To start an instance, run the following"
echo "    kubectl apply -n cockroach -f db.yaml"
echo "    kubectl apply -n cockroach -f client-secure-operator.yaml"

