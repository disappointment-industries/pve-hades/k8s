# VHC cluster setup

## Modify login.sh

in case git wants to track it:

```sh
git update-index --skip-worktree login.sh
```

## Usage

```sh
source login.sh
terraform apply -paralalism=2
```
