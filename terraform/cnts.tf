variable "cnts" {
  description = "Map of cnt definitions."
  type = map(any)
  default = {
    cloudflare-vpn = {
      storage_size = "20G",
      cores        = 4,
      memory = 4096
      ip4_pref =  "192.168.69",
      # haepuz_ip = "10.132.0.1/24"
      vmid = 111
    },
    # haepuz-jani-gateway = {
    #   storage_size = "50G",
    #   cores        = 6,
    #   memory = 8192,
    #   ip4_pref =  "192.168.69",
    #   haepuz_ip = "10.168.0.1/24"
    #   vmbr = "vmbr7",
    #   vmid = 191
    # },
  }
}

resource "proxmox_lxc" "cnts" {
  for_each = var.cnts

  provider    = proxmox.hades
  target_node = var.target_node
  hostname    = each.key
  ostemplate  = "local:vztmpl/ubuntu-20.04-standard_20.04-1_amd64.tar.gz"
  start       = true
  onboot      = true
  cores       = each.value.cores
  memory =  each.value.memory
  vmid        = each.value.vmid

  ssh_public_keys = var.ssh_keys

  rootfs {
    storage = var.storage
    size    = each.value.storage_size
  }

  network {
    name   = "eth0"
    bridge = "vmbr3"
    ip     = "${each.value.ip4_pref}.${each.value.vmid}/24"
    gw     = "${each.value.ip4_pref}.1"
  }

  network {
    name   = "eth1"
    bridge = "vmbr0"
    ip6    = "2001:738:2001:207f:0:211:131:${each.value.vmid}/96"
    gw6    = "fe80::"
  }
}

