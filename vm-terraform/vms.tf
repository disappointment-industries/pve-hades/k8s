variable "vms" {
  description = "Map of random VM definitions."
  type        = map(any)
  default = {
    testflix = {
      storage_size = "100G",
      cores        = 20,
      memory       = 16384,
      min_mem      = 4096,
      ip4_pref     = "192.168.69",
      ip6_pref     = "fd00",
      vmid         = 216,
      target_node  = "hades"
    }
  }
}

resource "proxmox_vm_qemu" "vms" {
  for_each = var.vms
  provider = proxmox.hades

  target_node = each.value.target_node
  name        = each.key
  desc        = "Terraform managed vm"
  vmid        = each.value.vmid

  bootdisk = "virtio0"

  clone      = "ubuntu-focal-cloudimg-template-${each.value.target_node}"
  full_clone = false

  agent = 1
  numa  = true

  cores   = each.value.cores
  sockets = 1
  vcpus   = each.value.cores
  memory  = each.value.memory
  balloon = each.value.min_mem

  disk {
    size     = each.value.storage_size
    type     = "virtio"
    storage  = var.storage-zfs
    discard  = "on"
    cache    = "unsafe"
    iothread = 1
  }

  network {
    model  = "virtio"
    bridge = "vmbr3"
  }

  network {
    model  = "virtio"
    bridge = "vmbr0"
  }

  ipconfig0 = "ip=${each.value.ip4_pref}.${each.value.vmid}/24,gw=192.168.69.1,ip6=${each.value.ip6_pref}::${each.value.vmid}/96"
  ipconfig1 = "ip6=2001:738:2001:207f:0:211:131:${each.value.vmid}/96,gw6=fe80::"

  sshkeys = var.ssh_keys
}