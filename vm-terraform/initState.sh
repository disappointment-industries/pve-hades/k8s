#!/bin/bash

source login.sh

PROJ_ID="27610296"
STATE_NAME="vms"

terraform init \
    -backend-config="address=https://gitlab.com/api/v4/projects/${PROJ_ID}/terraform/state/${STATE_NAME}" \
    -backend-config="lock_address=https://gitlab.com/api/v4/projects/${PROJ_ID}/terraform/state/${STATE_NAME}/lock" \
    -backend-config="unlock_address=https://gitlab.com/api/v4/projects/${PROJ_ID}/terraform/state/${STATE_NAME}/lock" \
    -backend-config="username=${GITLAB_USER}" \
    -backend-config="password=${GITLAB_REPO_PAT}" \
    -backend-config="lock_method=POST" \
    -backend-config="unlock_method=DELETE" \
    -backend-config="retry_wait_min=5" \
    -reconfigure