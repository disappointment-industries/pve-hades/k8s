terraform {
  required_providers {
    proxmox = {
      source  = "Telmate/proxmox"
      version = "2.7.1"
    }
  }

  backend "http" {
  }
}

provider "proxmox" {
  alias           = "hades"
  pm_api_url      = "https://pve.rethelyi.space/api2/json"
  pm_tls_insecure = true
  pm_timeout = 2400
}

variable "target_node" {
  type    = string
  default = "hades"
}

variable "storage-gluster-dir" {
  type    = string
  default = "awesome-gluster-dir"
}

variable "storage-zfs" {
  type    = string
  default = "local-zfs"
}

variable "ssh_keys" {
  type    = string
  default = <<EOF
  ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIO0Lx8viB76oK24/xv1uyDGvNaHF/jSY11nYbEqZmQDS blint@thankpad

  EOF
}
