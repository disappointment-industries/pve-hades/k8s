#!/bin/bash

python -m virtualenv venv
source venv/bin/activate

pip3 install -r kubespray-repo/requirements.txt

exec ${SHELL:-sh} "$@"