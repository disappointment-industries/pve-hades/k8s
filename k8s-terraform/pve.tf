terraform {
  required_providers {
    proxmox = {
      source  = "Telmate/proxmox"
      version = "2.7.1"
    }
  }

  backend "http" {
  }
}

provider "proxmox" {
  alias           = "hades"
  pm_api_url      = "https://pve.rethelyi.space/api2/json"
  pm_tls_insecure = true
  pm_timeout = 2400
}

variable "target_node" {
  type    = string
  default = "hades"
}

variable "storage" {
  type    = string
  default = "local-zfs"
}

variable "ssh_keys" {
  type    = string
  default = <<EOF
  ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQDmGSFboT9zDjNH8WJORAyygN0uneURIbktQdO4yaOAexhd99ZkKCCRH9ntp0B5u2ADfvWMXQ7ULiFobV0tgY0R6uWMGMsCl1ExrXtPwJe1foxlCRzrMMnym+LtfmEaD+CH5n9o3ikA0+PT1ScmZvmIaFvR+NgVYulctXlQ6S60rfnvCphpnfDSJhoRWwU2fun93/JHmKtieZH6yj+WvO7qWfRRm1wXnUQ72K/9EHhiu1CouXFjqpoGyvuDaO8RZnxm1acFR40/AqxtoMGtUZU2mDYYAP5CcRCRibrVVAJYL/AR5zcAiO3dxiBNImy22c5hj+5g++X8EuqWQLEwVC63SYwLIcFXobWph4OL405P2hVv3EM+luq3rrCeDrOV7yilWKiNNIoavWNvojsWjeED7PRczntHrHpPvZ9ohetXsHwySJ0yoDY5++m3jP9bD4QKq+z48mh3E1E0p6vmngL0BvO2hsWP1sDLJ3Dt8iF8s7gZW1fgqmjh7MmMTz7U4Ia8zub0fLV/eWfYGoY8jfmF2M6zwMkCRmt16SOrb4J2R2GHWBB5p6Cjz7iL8ePTdCCcxBqwXbNpor5tIc29ugYZ4sbw1Z/m3PbcXjI90cm52T2nsM7IeiWeHSQhsEW+qwEDdgMwhYuyMC2v51OAZVWuZCdxK2vtOOV6nQNwGP5K9w== thankpad

  EOF
}
