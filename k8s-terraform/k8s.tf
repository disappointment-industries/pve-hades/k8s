resource "proxmox_lxc" "k8s-mgmt" {
  provider    = proxmox.hades
  target_node = var.target_node
  hostname    = "k8s-mgmt"
  ostemplate  = "local:vztmpl/ubuntu-20.04-standard_20.04-1_amd64.tar.gz"
  start       = true
  onboot      = true
  cores       = 4
  vmid        = 180

  ssh_public_keys = var.ssh_keys

  rootfs {
    storage = var.storage
    size    = "16G"
  }

  network {
    name   = "eth0"
    bridge = "vmbr5"
    ip     = "172.24.0.2/16"
    gw     = "172.24.0.1"
    ip6    = "fd00::0:2/96"
  }

  network {
    name   = "eth1"
    bridge = "vmbr0"
    ip6    = "2001:738:2001:207f:0:211:132:c001/96"
    gw6    = "fe80::"
  }
}

variable "vms" {
  description = "Map of K8S VM definitions."
  type        = map(any)
  default = {
    k8s-master-01 = {
      storage_size = "16G",
      cores        = 4,
      memory       = 8192,
      min_mem      = 4096,
      ip           = "172.24.1.1/16",
      ip6          = "fd00::1:1/96",
      pip6         = "a001",
      vmid         = 281,
      target_node  = "hades"
    },
    k8s-worker-01 = {
      storage_size = "50G",
      cores        = 6,
      memory       = 16384,
      min_mem      = 4096,
      ip           = "172.24.2.1/16",
      ip6          = "fd00::2:1/96",
      pip6         = "b001",
      vmid         = 282,
      target_node  = "hades"
    },
    k8s-worker-02 = {
      storage_size = "50G",
      cores        = 6,
      memory       = 16384,
      min_mem      = 4096,
      ip           = "172.24.2.2/16",
      ip6          = "fd00::2:2/96",
      pip6         = "b002",
      vmid         = 283,
      target_node  = "hades"
    },
    k8s-awesome-slowboi = {
      storage_size = "50G",
      cores        = 4,
      memory       = 10240,
      min_mem      = 4096,
      ip           = "172.24.2.3/16",
      ip6          = "fd00::2:3/96",
      pip6         = "b003",
      vmid         = 284,
      target_node  = "slowboi"
    },
    k8s-awesome-hades = {
      storage_size = "50G",
      cores        = 6,
      memory       = 16384,
      min_mem      = 4096,
      ip           = "172.24.2.4/16",
      ip6          = "fd00::2:4/96",
      pip6         = "b004",
      vmid         = 285,
      target_node  = "hades"
    }
  }
}


resource "proxmox_vm_qemu" "vms" {
  for_each = var.vms
  provider = proxmox.hades

  target_node = each.value.target_node
  name        = each.key
  desc        = "Terraform managed vm"
  vmid        = each.value.vmid

  bootdisk = "virtio0"

  clone      = "ubuntu-focal-cloudimg-template-${each.value.target_node}"
  full_clone = false
  #additional_wait = "120"

  agent = 1
  numa  = true

  cores   = each.value.cores
  sockets = 1
  vcpus   = each.value.cores
  memory  = each.value.memory
  balloon = each.value.min_mem

  disk {
    size     = each.value.storage_size
    type     = "virtio"
    storage  = var.storage
    discard  = "on"
    cache    = "unsafe"
    iothread = 1
  }

  network {
    model  = "virtio"
    bridge = "vmbr5"
  }

  network {
    model  = "virtio"
    bridge = "vmbr0"
  }

  ipconfig0 = "ip=${each.value.ip},gw=172.24.0.1,ip6=${each.value.ip6}"
  # ipconfig1 = "ip6=2001:738:2001:207f:0:211:132:${each.value.pip6}/96,gw6=fe80::"

  sshkeys = var.ssh_keys
}

