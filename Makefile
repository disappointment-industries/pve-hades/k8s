all: pre k8s post

cluster: all

pre:
	echo Running pre-k8s tasks
	bash -c "cd pre-k8s; ./bootstrap.sh -c 'ansible-playbook setup_mgmt.yaml'"

k8s:
	echo Creating cluster
	ssh -A -t mgmt.k8s.valkyrie 'bash -c "cd kubespray/kubespray-repo; ansible-playbook -i ../inventory/valkyrie/hosts.yml cluster.yml"'

reset:
	echo Resetk cluster
	ssh -A -t mgmt.k8s.valkyrie 'bash -c "cd kubespray/kubespray-repo; ansible-playbook -i ../inventory/valkyrie/hosts.yml reset.yml"'

post:
	echo Running post-k8s tasks
	bash -c "cd post-k8s; ./bootstrap.sh -c 'ansible-playbook post_cluster.yaml'"

